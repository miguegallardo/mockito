package org.uma.mockito;

import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by ajnebro on 9/3/16.
 */
public class pruebaListMockito {
  @Test (expected = RuntimeException.class)
  //En este Test comprobaremos el funcionamiento del metodo get(2)

  public void pruebaLista1() {
    // Creating the mock object
    List<String> list = mock(List.class) ;

    // Stubbing: defining the behavior
    when(list.get(0)).thenReturn("First") ;
    when(list.get(1)).thenReturn("Second") ;
    when(list.get(2)).thenThrow(new RuntimeException()) ;
    when(list.get(3)).thenReturn("Third") ;

    // Using the mock object
    System.out.println(list.get(0));
    System.out.println(list.get(1));
    System.out.println(list.get(2));
    System.out.println(list.get(3));

    // Verifying
    verify(list, times(1)).get(0) ;
    verify(list, times(1)).get(1);
    verify(list, times(1)).get(3);
  }

    @Test
    //En este Test comprobaremos el funcionamiento del metodo get(3)

    public void pruebaLista2() {
        // Creating the mock object
        List<String> list = mock(List.class) ;

        // Stubbing: defining the behavior
        when(list.get(0)).thenReturn("First") ;
        when(list.get(1)).thenReturn("Second") ;
        //when(list.get(2)).thenThrow(new RuntimeException()) ;
        when(list.get(3)).thenReturn("Third") ;

        // Using the mock object
        System.out.println(list.get(0));
        System.out.println(list.get(1));
        //System.out.println(list.get(2));
        System.out.println(list.get(3));

        // Verifying
        verify(list, times(1)).get(0) ;
        verify(list, times(1)).get(1);
        verify(list, times(1)).get(3);
    }
}
